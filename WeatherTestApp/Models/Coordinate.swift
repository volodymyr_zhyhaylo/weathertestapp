//
//  Coordinate.swift
//  WeatherTestApp
//
//  Created on 27.02.2019.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import Foundation
import CoreLocation

struct Coordinate {
    public var latitude: Double
    public var longitude: Double
    
    static var sharedInstance = Coordinate(latitude: 0.0, longitude: 0.0)
    
    static let locationManager = CLLocationManager()
    
    typealias CheckLocationPermissionsCompletionHandler = (Bool) -> Void
    static func checkForGrantedLocationPermissions(completionHandler completion: @escaping CheckLocationPermissionsCompletionHandler) {
        let locationPermissionsStatusGranted = CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        
        if locationPermissionsStatusGranted {
            guard let currentLocation = locationManager.location else {return}
            Coordinate.sharedInstance.latitude = currentLocation.coordinate.latitude
            Coordinate.sharedInstance.longitude = currentLocation.coordinate.longitude
        }
        
        completion(locationPermissionsStatusGranted)
    }
    
   
}

//
//  OpenWeatherMapError.swift
//  WeatherTestApp
//
//  Created on 27.02.2019.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import Foundation

enum OpenWeatherMapError: Error {
    case requestFailed
    case responseUnsuccessful
    case invalidaData
    case jsonConversionFailure
    case invalidUrl
    case jsonParsingFailure
}
